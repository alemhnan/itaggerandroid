package it.itcveneto.itagger;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

import it.itcveneto.restclient.Tag;
import it.itcveneto.restclient.TaggerResponse;
import it.itcveneto.restclient.iTaggerClient;

public class MainActivity extends ActionBarActivity {

    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds

    protected android.location.LocationManager locationManager;

    public void getTags(View view) {
        ListView lv = (ListView) findViewById(R.id.taglist);
        Location location = locationManager.getLastKnownLocation(android.location.LocationManager.GPS_PROVIDER);

        EditText editDistance = (EditText) findViewById(R.id.distance);
        String distanceStr = editDistance.getText().toString();
        int distance = Integer.parseInt(distanceStr) * 1000;
        Log.d("iTagger", "distance: " + distance);



        if (location != null) {
            TaggerResponse tResponse = iTaggerClient.getTags(location.getLatitude(), location.getLongitude() , distance);
            List<Tag> tags = tResponse.getData();
            ArrayList<String> items = new ArrayList<String>();
            for(Tag tag : tags){
                items.add(tag.getTagname() + " lat:" + tag.getLat() + " long:" + tag.getLngt());
            }

            // http://stackoverflow.com/questions/5070830/populating-a-listview-using-arraylist
            // This is the array adapter, it takes the context of the activity as a first // parameter, the type of list view as a second parameter and your array as a third parameter
            ArrayAdapter<String> arrayAdapter =
                    new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, items);
            lv.setAdapter(arrayAdapter);


        }

    }


        /** Called when the user clicks the Send button */
    public void postTag(View view) {

        EditText editText = (EditText) findViewById(R.id.tagName);
        String tagName = editText.getText().toString();
        Log.d("iTagger", "Tag name: " + tagName);


        Location location = locationManager.getLastKnownLocation(android.location.LocationManager.GPS_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \t Longitude: %1$s \t Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            Log.d("iTagger", message);


            iTaggerClient.postTag(location.getLatitude(), location.getLongitude() , tagName);



        } else {
            Log.d("iTagger", "No location!!");
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //http://www.javacodegeeks.com/2010/09/android-location-based-services.html
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()
        );


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
        }
    }


    //http://www.javacodegeeks.com/2010/09/android-location-based-services.html
    private class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {
            String message = String.format(
                    "New Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            Log.d("iTagger", "Location: " + message);

        }

        public void onStatusChanged(String s, int i, Bundle b) {
            Log.d("iTagger", "Provider status changed");
        }

        public void onProviderDisabled(String s) {
            Log.d("iTagger", "Provider disabled by the user. GPS turned off");
        }

        public void onProviderEnabled(String s) {
            Log.d("iTagger", "Provider enabled by the user. GPS turned on");
        }

    }


}
