package it.itcveneto.restclient;

public class Tag {
	
	private int _id;
	private String _tagname;
	private double _lat;
	private double _lngt;
	private boolean _deleted;

	public int getID() { return _id; }
	public void setID(int i) { _id = i; }

	public String getTagname() { return _tagname; }
	public void setTagname(String t) { _tagname = t; }

	public double getLat() { return _lat; }
	public void setLat(double l) { _lat = l; }

	public double getLngt() { return _lngt; }
	public void setLngt(double lngt) { _lngt = lngt; }

	public boolean getDeleted() { return _deleted; }
	public void setDeleted(boolean d) { _deleted = d; }

	
}