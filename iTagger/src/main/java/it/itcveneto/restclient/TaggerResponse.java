package it.itcveneto.restclient;

import java.util.List;

public class TaggerResponse {
	
	private String _message;
	public String getMessage() { return _message; }
	public void setMessage(String m) { _message = m; }
	
	private List<Tag> _data;
	public List<Tag> getData() { return _data; }
	public void setData(List<Tag> d) { _data = d; }

	

}
