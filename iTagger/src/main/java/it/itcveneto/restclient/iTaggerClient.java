package it.itcveneto.restclient;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.databind.ObjectMapper;

public class iTaggerClient {
	
	private static final String baseUrl  = "http://desolate-tor-3374.herokuapp.com/";
	private static final String resource = "tags";

	
	public static TaggerResponse getTags(double _lat, double _lngt, double max) {
        String lat = Double.toString(_lat);
        String lngt = Double.toString(_lngt);

		String url = baseUrl + resource + "?" + 
		"lat=" + lat + 
		"&" + "lngt=" + lngt +
		"&" + "max="  + max;

		try {
			URL obj = new URL(url);
			HttpURLConnection con;

			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			if(responseCode != 200) {
				return null;
			};
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
				 
			ObjectMapper mapper = new ObjectMapper();

			TaggerResponse myTags = mapper.readValue(in, TaggerResponse.class);
			return myTags;
		} catch (IOException e) {
			return null;
		} catch (Exception e){
			return null;
		}

	}

	
	public static void postTag(double _lat, double _lngt, String tag){
        String lat = Double.toString(_lat);
        String lngt = Double.toString(_lngt);

        String url = baseUrl + resource;

		URL obj;
		try {
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");

			// Send post request
			con.setDoOutput(true);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());

			String params = "{\"id\": -1," +
					"\"tagname\": \""+ tag + "\", " +
					"\"lat\": "+ lat +" , " +
					"\"lngt\": " + lngt + "}";


			wr.writeBytes(params );
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
//			System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Response Code : " + responseCode);
            Log.d("iTagger",  "Response Code : " + responseCode);



        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            //System.out.println("ops!!");

        }

	}
	
}
